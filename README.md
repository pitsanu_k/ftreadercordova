### Feitian iR301-U Android Cordova plugin for Thai Id Card ###
* Cordova plugin add {src_path}/app/com.appman.ftusb.FtUsb

### Usage ###
* FtUsb.openReader for connect reader
* FtUsb.readAll for read personal data as json object
* FtUsb.stop to disconnect reader

### Json Object ###
* id - string
* infoTh - string
* infoEn - string
* birthdate - string
* sex - string
* address - string
* addressNo - string
* villageNo - string
* lane - string
* building - string
* road - string
* tambol - string
* amphur - string
* province - string
* card_issue - string
* card_expire - string
* photo - base64