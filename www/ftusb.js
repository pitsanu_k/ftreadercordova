var argscheck = require('cordova/argscheck'),
    utils = require('cordova/utils'),
    exec = require('cordova/exec');

var deviceType = (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";

var FtUsb = function()
{

};

FtUsb.init = function(success, error)
{
	var successCallback = function(result)
	{
		if (success)
		{
			success(result);
		}
	}, 
	failureCallback = function(result)
	{
		if (error)
		{
			error(result);
		}
	};
	exec
	(
		successCallback,
		failureCallback,
		"FtUsb",
		"init",
		[]
	);
};

FtUsb.stop = function(success, error)
{
	var successCallback = function(result)
	{
		if (success)
		{
			success(result);
		}
	}, 
	failureCallback = function(result)
	{
		if (error)
		{
			error(result);
		}
	};
	exec
	(
		successCallback,
		failureCallback,
		"FtUsb",
		"stop",
		[]
	);
};

FtUsb.openReader = function(success, error)
{
	var successCallback = function(result)
	{
		if (success)
		{
			success(result);
		}
	}, 
	failureCallback = function(result)
	{
		if (error)
		{
			error(result);
		}
	};
	exec
	(
		successCallback,
		failureCallback,
		"FtUsb",
		"openReader",
		[]
	);
};

FtUsb.readAll = function(success, error)
{
	var successCallback = function(result)
	{
		if (success)
		{
			success(result);
		}
	}, 
	failureCallback = function(result)
	{
		if (error)
		{
			error(result);
		}
	};
	exec
	(
		successCallback,
		failureCallback,
		"FtUsb",
		"readAll",
		[]
	);
};

module.exports = FtUsb;
