package com.appman.ftusb.models;

import android.text.TextUtils;
import android.util.Base64;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CardModel {
    private String id = "";
    private String info = "";
    private String prefixTh = "";
    private String firstnameTh = "";
    private String middleTh = "";
    private String lastnameTh = "";
    private String prefixEn = "";
    private String firstnameEn = "";
    private String middleEn = "";
    private String lastnameEn = "";
    private String birthdate = "";
    private String sex = "";
    private String orgAddress = "";
    private String[] addressArray = {"", "", "", "", "", "", "", ""};
    private String expire = "";
    private String issue = "";
    private byte[] photo = null;

    public String getId() {
        if (id == null || id.length() < 13)
            return id;
        return id.substring(0, 13).trim();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInfoTh() {
        return TextUtils.join(" ", new String[]{ prefixTh, firstnameTh, middleTh, lastnameTh });
    }

    public String getInfoEn() {
        return TextUtils.join(" ", new String[]{ prefixEn, firstnameEn, middleEn, lastnameEn });
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getSex() {
        return sex;
    }

    public void setInfo(String info) {
        this.info = info;

        String[] infoArray = info.substring(0, 100).trim().split("#");
        prefixTh = infoArray[0];
        firstnameTh = infoArray[1];
        middleTh = infoArray[2];
        lastnameTh = infoArray[3];

        infoArray = info.substring(100, 200).trim().split("#");
        prefixEn = infoArray[0];
        firstnameEn = infoArray[1];
        middleEn = infoArray[2];
        lastnameEn = infoArray[3];

        birthdate = info.substring(200, 208).replace("#", " ").trim();
        sex = info.substring(208, 209).replace("#", " ").trim();
    }

    public String getOrgAddress() {
        return orgAddress;
    }

    public String getAddress() {
        return TextUtils.join(" ", new String[] {
                getAddressNo(), getVillageNo(), getLane(), getBuilding(), getRoad(),
                getTambol(), getAmphur(), getProvince() });
    }

    public void setAddress(String address) {
        orgAddress = address;
        addressArray = address.trim().split("#");
    }

    public String getAddressNo() {
        return addressArray[0].replaceAll("\\s", "");
    }

    public String getVillageNo() {
        return addressArray[1].replaceAll("\\s", "");
    }

    public String getLane() {
        return addressArray[2].replaceAll("\\s", "");
    }

    public String getBuilding() {
        return addressArray[3].replaceAll("\\s", "");
    }

    public String getRoad() {
        return addressArray[4].replaceAll("\\s", "");
    }

    public String getTambol() {
        return addressArray[5].replaceAll("\\s", "");
    }

    public String getAmphur() {
        return addressArray[6].replaceAll("\\s", "");
    }

    public String getProvince() {
        String province = addressArray[7];

        int spaceIndex = 0;
        int charCode;
        for (int i = 0; i < province.length(); i++) {
            charCode = province.charAt(i) - '0';
            if (charCode == -16 || charCode == 96 || charCode == -48 || charCode == -2) {
                spaceIndex = i;
                break;
            }
        }

        if (spaceIndex <= 0)
            return province;
        return province.substring(0, spaceIndex).trim();
    }

    public void setIssueExpire(String issueExpire) {
        issue = issueExpire.substring(0, 8);
        expire = issueExpire.substring(8, 16);
    }

    public String getExpire() {
        return expire;
    }

    public String getIssue() {
        return issue;
    }

    public byte[] getPhoto() {
        if (photo == null)
            return null;
        return Arrays.copyOf(photo, photo.length);
    }

    public String getPhotoBase64() {
        if (photo == null)
            return null;
        return Base64.encodeToString(photo, Base64.NO_WRAP);
    }

    public void setPhoto(byte[] photoBytes) {
        photo = photoBytes;
    }

    public Map<String, String> getObjectMap() {
        Map<String, String> mapper = new HashMap<String, String>();
        mapper.put("id", getId());
        mapper.put("infoTh", getInfoTh());
        mapper.put("infoEn", getInfoEn());
        mapper.put("birthdate", getBirthdate());
        mapper.put("sex", getSex());
        mapper.put("address", getAddress());
        mapper.put("addressNo", getAddressNo());
        mapper.put("villageNo", getVillageNo());
        mapper.put("lane", getLane());
        mapper.put("building", getBuilding());
        mapper.put("road", getRoad());
        mapper.put("tambol", getTambol());
        mapper.put("amphur", getAmphur());
        mapper.put("province", getProvince());
        mapper.put("card_issue", getIssue());
        mapper.put("card_expire", getExpire());
        mapper.put("photo", getPhotoBase64());
        return mapper;
    }

    @Override
    public String toString() {
        return TextUtils.join("\n", new String[] {
                String.format("id: %s", id), String.format("info: %s", info),
                String.format("birthdate: %s", birthdate), String.format("sex: %s", sex),
                String.format("address: %s", getAddress()),
                String.format("issue: %s", issue), String.format("expire: %s", expire)});
    }
}
