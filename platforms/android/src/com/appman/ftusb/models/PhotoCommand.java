package com.appman.ftusb.models;

public class PhotoCommand {
    byte[] cmd1, cmd2;

    public byte[] getCmd1() {
        return cmd1;
    }

    public void setCmd1(byte[] cmd1) {
        this.cmd1 = cmd1;
    }

    public byte[] getCmd2() {
        return cmd2;
    }

    public void setCmd2(byte[] cmd2) {
        this.cmd2 = cmd2;
    }
}
