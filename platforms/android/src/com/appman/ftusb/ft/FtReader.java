package com.appman.ftusb.ft;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Handler;

import com.feitian.reader.devicecontrol.Card;

public class FtReader {
    private boolean isPowerOn = true;
    private Card innerCard;

    public FtReader(UsbManager mUsbManager, UsbDevice mDevice) {
        // public Card(UsbManager mManager, UsbDevice mDev,Handler mHandler)
        innerCard = new Card(mUsbManager, mDevice, null);
        // TODO Auto-generated constructor stub
    }

    public String getReaderName() {
        return innerCard.getReaderName();
    }

    public String getManufacturerName() {
        return innerCard.getManufacturerName();
    }

    public int open() throws FtBlueReadException {
        int ret = innerCard.open();
        if (ret != STATUS.RETURN_SUCCESS) {
            if (ret == STATUS.READER_NOT_SUPPORT) {
                throw new FtBlueReadException(
                        "sorry we just support FeiTian reader");
            }
            throw new FtBlueReadException("open device error");
        }
        return ret;
    }

    public boolean isPowerOn() {
        return isPowerOn;
    }

    public int PowerOn() throws FtBlueReadException {
        // if(innerCard.getcardStatus() == STATUS.CARD_ABSENT){
        // throw new FtBlueReadException("card is absent");
        // }
        int ret = innerCard.PowerOn();
        if (ret != STATUS.RETURN_SUCCESS) {
            throw new FtBlueReadException("Power On Failed");
        }
        isPowerOn = true;
        return ret;
    }

    public int PowerOff() throws FtBlueReadException {
        int ret = innerCard.PowerOff();
        if (ret != STATUS.RETURN_SUCCESS) {
            throw new FtBlueReadException("Power On Failed");
        }
        isPowerOn = false;
        return ret;
    }

    public int PowerRestart() throws FtBlueReadException {
        int ret = innerCard.PowerOff();
        if (ret != STATUS.RETURN_SUCCESS) {
            throw new FtBlueReadException("Power Restart Failed");
        }

        ret = innerCard.PowerOn();
        if (ret != STATUS.RETURN_SUCCESS) {
            throw new FtBlueReadException("Power Restart Failed");
        }

        return ret;
    }

    public String getDkVersion(){
        return innerCard.GetDkVersion();
    }

    public int getVersion(byte []recvBuf,int []recvBufLen){
        return innerCard.getVersion(recvBuf, recvBufLen);
    }

    public int getSerialNum(byte[] serial,int serialLen[]){
        return innerCard.FtGetSerialNum(serial, serialLen);
    }

    public int readFlash(byte[] buf,int offset,int len){
        return innerCard.FtReadFlash(buf, offset, len);
    }

    public int writeFlash(byte[] buf,int offset,int len){
        return innerCard.FtWriteFlash(buf, offset, len);
    }

    public int getProtocol() throws FtBlueReadException {
        if (!isPowerOn) {
            throw new FtBlueReadException("Power Off already");
        }
        return innerCard.getProtocol();
    }

    public byte[] getAtr() throws FtBlueReadException {
        if (!isPowerOn) {
            throw new FtBlueReadException("Power Off already");
        }
        return innerCard.getAtr();
    }

    public int getKeySn(byte[] buffer, int[] bufferLen) throws FtBlueReadException {
        if (!isPowerOn) {
            throw new FtBlueReadException("Power Off already");
        }
        return innerCard.GetKeySn(buffer, bufferLen);
    }

    // nLen accept only 40 or 48
    public int initDukpt(byte[] encBuf, int nLen) throws FtBlueReadException {
        if (!isPowerOn) {
            throw new FtBlueReadException("Power Off already");
        }
        return innerCard.FtInitDukpt(encBuf, nLen);
    }

    public int getCardStatus() {
        return innerCard.getcardStatus();
    }

    public void startCardStatusMonitoring(Handler Handler)
            throws FtBlueReadException {
        if (innerCard.registerCardStatusMonitoring(Handler) != 0) {
            throw new FtBlueReadException("not support cardStatusMonitoring");
        }
    }

    public int transApdu(int tx_length, final byte tx_buffer[],
                         int rx_length[], final byte rx_buffer[]) throws FtBlueReadException {
        if (!isPowerOn) {
            throw new FtBlueReadException("Power Off already");
        }

        int ret = innerCard.transApdu(tx_length, tx_buffer, rx_length,
                rx_buffer);

        if (ret == STATUS.BUFFER_NOT_ENOUGH) {
            throw new FtBlueReadException("receive buffer not enough");
        } else if (ret == STATUS.TRANS_RETURN_ERROR) {
            throw new FtBlueReadException("trans apdu error");
        }
        return ret;
    }

    public int close() {
        return innerCard.close();
    }
}
