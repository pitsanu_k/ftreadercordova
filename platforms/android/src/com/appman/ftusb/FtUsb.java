package com.appman.ftusb;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.appman.ftusb.apdu.IAPDU_THAI_IDCARD;
import com.appman.ftusb.apdu.THAI_IDCARD_3B67;
import com.appman.ftusb.apdu.THAI_IDCARD_3B68;
import com.appman.ftusb.ft.FtBlueReadException;
import com.appman.ftusb.ft.FtReader;
import com.appman.ftusb.ft.STATUS;
import com.appman.ftusb.models.CardModel;
import com.appman.ftusb.models.PhotoCommand;
import com.feitian.readerdk.Tool.DK;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FtUsb extends CordovaPlugin implements Handler.Callback {
    private static final String ACTION_USB_PERMISSION = "com.appman.ftusb.USB_PERMISSION";
    private static final String LOG_TAG = "BatteryManager";
    private BroadcastReceiver mUsbReceiver;
    private UsbManager mUsbManager;
    private IntentFilter mUsbFilter;
    private PendingIntent mPermissionIntent;
    private List<String> mDeviceList = new ArrayList<String>();
    private FtReader mReader;
    private IAPDU_THAI_IDCARD mApduCard;
    private Handler mHandler;

    public FtUsb() {
        mUsbReceiver = null;
    }

    @Override
    protected void pluginInitialize() {
        Log.i(LOG_TAG, "pluginInitialize");

        super.pluginInitialize();
        Activity activity = cordova.getActivity();
        mHandler = new Handler(Looper.getMainLooper(), this);
        mUsbManager = (UsbManager) activity.getSystemService(Context.USB_SERVICE);
        mPermissionIntent = PendingIntent.getBroadcast(activity, 0, new Intent(ACTION_USB_PERMISSION), 0);

        mUsbFilter = new IntentFilter();
        mUsbFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        mUsbFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
    }

    @Override
    public boolean execute(final String action, final JSONArray args, final CallbackContext callbackContext) {
        Log.i(LOG_TAG, "execute: " + action);
        cordova.getThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.equals(action, "stop"))
                    stop(callbackContext);
                else if (TextUtils.equals(action, "openReader"))
                    openReader(callbackContext);
                else if (TextUtils.equals(action, "readAll"))
                    readAll(callbackContext);
                else
                    callbackContext.error("No action support.");
            }
        });

        return true;
    }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);
        mUsbReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                checkUsbDeviceStatus(intent);
            }
        };
        cordova.getActivity().registerReceiver(mUsbReceiver, mUsbFilter);
    }

    @Override
    public void onPause(boolean multitasking) {
        super.onPause(multitasking);
        removeUsbReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        removeUsbReceiver();
    }

    @Override
    public void onReset() {
        super.onReset();
        removeUsbReceiver();
    }

    @Override
    public boolean handleMessage(Message msg) {
        if (msg.what != DK.CARD_STATUS)
            return false;

        switch (msg.arg1) {
            case DK.CARD_ABSENT:
                powerOff();
                return true;
            case DK.CARD_PRESENT:
                powerOn();
                return true;
            case DK.CARD_UNKNOWN:
            case DK.IFD_COMMUNICATION_ERROR:
                return true;
        }

        return false;
    }

    private void powerOn() {
        try {
            mReader.PowerOn();
            mApduCard = initApduCard(mReader.getAtr());
        } catch (Exception e) {}
    }

    private void powerOff() {
        try {
            mReader.PowerOff();
        } catch (Exception e) {}
    }

    private void stop(CallbackContext callbackContext) {
        Log.i(LOG_TAG, "stop");
        try {
            mReader.close();
            callbackContext.success();
        } catch (Exception e) {
            callbackContext.error(e.toString());
        }
    }

    private void removeUsbReceiver() {
        if (mUsbReceiver != null) {
            try {
                cordova.getActivity().unregisterReceiver(mUsbReceiver);
                mUsbReceiver = null;
            } catch (Exception e) {
                Log.e(LOG_TAG, "Error unregistering usb receiver: " + e.getMessage(), e);
            }
        }
    }

    private void checkUsbDeviceStatus(Intent intent) {
        String action = intent.getAction();
        if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
            onUsbDeviceAttached(intent);
        } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
            onUsbDeviceDetached(intent);
        }
    }

    private void onUsbDeviceAttached(Intent intent) {
        createDevicesList();
        UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        if (device != null) {
            Log.i(LOG_TAG, String.format("Add:  DeviceName: %s DeviceProtocol: %s\n", device.getDeviceName(), device.getDeviceProtocol()));
        }
    }

    private void onUsbDeviceDetached(Intent intent) {
        if (mReader != null) {
            mReader.close();
        }

        UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
        if (device != null) {
            Log.i(LOG_TAG, String.format("Del:  DeviceName: %s DeviceProtocol: %s\n", device.getDeviceName(), device.getDeviceProtocol()));
        }
    }

    private void createDevicesList() {
        mDeviceList.clear();
        for (UsbDevice device : mUsbManager.getDeviceList().values()) {
            mDeviceList.add(device.getDeviceName());
        }
    }

    private void openReader(CallbackContext callbackContext) {
        Log.i(LOG_TAG, "openReader");
        if (mDeviceList.isEmpty()) {
            createDevicesList();
        }

        if (mDeviceList.isEmpty()) {
            callbackContext.error("No device connect.");
            return;
        }

        String selectedDevice = mDeviceList.get(0);
        UsbDevice device = mUsbManager.getDeviceList().get(selectedDevice);

        if (!mUsbManager.hasPermission(device)) {
            mUsbManager.requestPermission(device, mPermissionIntent);
        }

        if (!mUsbManager.hasPermission(device)) {
            callbackContext.error("No permission");
            return;
        }

        if (mReader != null) {
            mReader.close();
        }

        mReader = new FtReader(mUsbManager, device);

        try {
            mReader.open();
            mReader.startCardStatusMonitoring(mHandler);

            Map<String, String> mapper = new HashMap<String, String>();
            mapper.put("manufacturer", mReader.getManufacturerName());
            mapper.put("reader", mReader.getReaderName());
            mapper.put("dk_version", mReader.getDkVersion());

            byte readerVersion[] = new byte[512];
            int[] len = new int[1];
            int result = mReader.getVersion(readerVersion, len);
            if (result == STATUS.RETURN_SUCCESS)
                mapper.put("version", readerVersion[0] + "." + readerVersion[1]);
            else
                mapper.put("version", "");

            callbackContext.success(new JSONObject(mapper));
        } catch (Exception e) {
            callbackContext.error(e.getMessage());
        }
    }

    private void readAll(CallbackContext callbackContext) {
        Log.i(LOG_TAG, "readAll");
        try {
            powerOn();

            byte[] array = new byte[256];
            SendCommand(mApduCard.CMD_SELECT(), array);
            CardModel card = new CardModel();

            int ret;
            ret = SendCommand(mApduCard.CMD_CID(), array); //mReader.transApdu(iApdu.length, arrayOfByte, receiveln, array);
            if (ret == STATUS.RETURN_SUCCESS) {
                card.setId(new String(array, "TIS-620"));
            }

            ret = SendCommand(mApduCard.CMD_PERSON_INFO(), array);
            if (ret == STATUS.RETURN_SUCCESS) {
                card.setInfo(new String(array, "TIS-620"));
            }

            ret = SendCommand(mApduCard.CMD_ADDRESS(), array);
            if (ret == STATUS.RETURN_SUCCESS) {
                card.setAddress(new String(array, "TIS-620"));
            }

            ret = SendCommand(mApduCard.CMD_CARD_ISSUE_EXPIRE(), array);
            if (ret == STATUS.RETURN_SUCCESS) {
                card.setIssueExpire(new String(array, "TIS-620"));
            }

            byte[] photo = SendPhotoCommand();
            card.setPhoto(photo);

            callbackContext.success(new JSONObject(card.getObjectMap()));
        } catch (Exception e) {
            callbackContext.error(e.toString());
        }
    }

    private IAPDU_THAI_IDCARD initApduCard(byte[] atr) throws FtBlueReadException {
        if (atr[0] == 0x3B && (atr[1] == 0x68 || atr[1] == 0x78)) {
            return new THAI_IDCARD_3B68();
        } else if (atr[0] == 0x3B && atr[1] == 0x67) {
            return new THAI_IDCARD_3B67();
        } else {
            throw new FtBlueReadException("Card not support");
        }
    }

    private int SendCommand(byte[][] commands, byte[] pbRecvBuffer) throws FtBlueReadException {
        int[] receiveln = new int[2];
        int result = -1;
        for (byte[] command : commands) {
            result = mReader.transApdu(command.length, command, receiveln, pbRecvBuffer);
        }

        return result;
    }

    private byte[] SendPhotoCommand() throws FtBlueReadException {
        ArrayList<PhotoCommand> photoCommands = mApduCard.GET_CMD_CARD_PHOTO();
        byte[] commandBytes, recv;

        int[] receiveln = new int[2];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        for (PhotoCommand command : photoCommands) {
            recv = new byte[256];
            commandBytes = command.getCmd1();
            mReader.transApdu(commandBytes.length, commandBytes, receiveln, recv);
            if (recv.length > 0) {
                recv = new byte[256];
                commandBytes = command.getCmd2();
                mReader.transApdu(commandBytes.length, commandBytes, receiveln, recv);
                bos.write(recv, 0, recv.length - 2);
            }
        }

        return bos.toByteArray();
    }
}
