package com.appman.ftusb.apdu;

import com.appman.ftusb.models.PhotoCommand;

import java.util.ArrayList;

public abstract class IAPDU_THAI_IDCARD {
    public abstract byte[][] CMD_CID();
    public abstract byte[][] CMD_PERSON_INFO();
    public abstract byte[][] CMD_ADDRESS();
    public abstract byte[][] CMD_CARD_ISSUE_EXPIRE();

    public byte[][] CMD_SELECT() {
        return new byte[][] {
                new byte[] {
                        (byte) 0x00, (byte) 0xA4, (byte) 0x04, (byte) 0x00, (byte) 0x08, (byte) 0xA0,
                        (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x54, (byte) 0x48, (byte) 0x00,
                        (byte) 0x01 }
        };
    }

    // photo
    public ArrayList<PhotoCommand> GET_CMD_CARD_PHOTO() {
        ArrayList<PhotoCommand> cmds = new ArrayList<PhotoCommand>();
        PhotoCommand command;
        for (int i = 0; i <= 20; i++) {
            command = new PhotoCommand();
            int xwd;
            int xof = i * 254 + 379;
            if (i == 20)
                xwd = 38;
            else
                xwd = 254;

            int sp2 = (xof >> 8) & 0xff;
            int sp3 = xof & 0xff;
            int sp6 = xwd & 0xff;
            int spx = xwd & 0xff;

            command.setCmd1(new byte[]{(byte) 0x80, (byte) 0xb0, (byte) sp2, (byte) sp3, 0x02, 0x00, (byte) sp6});
            command.setCmd2(new byte[]{(byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) spx});
            cmds.add(command);
        }

        return cmds;
    }
}
