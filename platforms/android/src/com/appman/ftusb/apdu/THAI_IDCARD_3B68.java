package com.appman.ftusb.apdu;

public class THAI_IDCARD_3B68 extends IAPDU_THAI_IDCARD {
    public byte[][] CMD_CID() {
        return new byte[][] {
                new byte[]{ (byte) 0x80, (byte) 0xb0, (byte) 0x00, (byte) 0x04, (byte) 0x02, (byte) 0x00, (byte) 0x0d },
                new byte[]{ (byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) 0x0d } // 00 c0 00 00 0d
        };
    }

    // Fullname Thai + Eng + BirthDate + Sex
    public byte[][] CMD_PERSON_INFO() {
        return new byte[][]{
                new byte[]{ (byte) 0x80, (byte) 0xb0, (byte) 0x00, (byte) 0x11, (byte) 0x02, (byte) 0x00, (byte) 0xd1 },
                new byte[]{ (byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) 0xd1 }  // 00 c0 00 00 d1
        };
    }

    // Address
    public byte[][] CMD_ADDRESS() {
        return new byte[][]{
                new byte[]{ (byte) 0x80, (byte) 0xb0, (byte) 0x15, (byte) 0x79, (byte) 0x02, (byte) 0x00, (byte) 0x64 },
                new byte[]{ (byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) 0x64 } // 00 c0 00 00 64
        };
    }

    // issue/expire
    public byte[][] CMD_CARD_ISSUE_EXPIRE() {
        return new byte[][]{
                new byte[]{ (byte) 0x80, (byte) 0xb0, (byte) 0x01, (byte) 0x67, (byte) 0x02, (byte) 0x00, (byte) 0x12 },
                new byte[]{ (byte) 0x00, (byte) 0xc0, (byte) 0x00, (byte) 0x00, (byte) 0x12 } // 00 c0 00 00 12
        };
    }
}
